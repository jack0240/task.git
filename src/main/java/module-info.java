/**
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 * 模块化管理，每增加一个包，都要在这里配置！
 */
module cn.jack1996 {
    // 声明模块依赖
    requires javafx.controls;
    requires javafx.fxml;

    // 开发模块内的包 [到 指定模块列表]
    opens cn.jack1996.task to javafx.fxml;
    opens cn.jack1996.fx.p01 to javafx.fxml;

    // 导出模块内的包 [到 指定模块列表]
    exports cn.jack1996.task;
    exports cn.jack1996.fx.p01;
}