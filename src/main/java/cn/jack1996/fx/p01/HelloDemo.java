/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jack1996.fx.p01;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Hello World页面展示
 *
 * @author Jack魏
 * @since 2023/6/13 22:42
 */
public class HelloDemo extends Application {
    /**
     * 程序主入口
     * 如果不在module-info.java里面配置模块信息，就会报错：
     * Caused by: java.lang.IllegalAccessException: class com.sun.javafx.application.LauncherImpl (in module javafx
     * .graphics) cannot access class cn.jack1996.fx.p01.HelloDemo (in module cn.jack1996) because module cn.jack1996
     * does not export cn.jack1996.fx.p01 to module javafx.graphics
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * 开始
     *
     * @param stage 舞台
     */
    @Override
    public void start(Stage stage) {
        Label label = new Label();
        label.setText("Hello World");
        StackPane root = new StackPane();
        root.getChildren().add(label);
        Scene scene = new Scene(root, 800, 600);
        stage.setTitle("Demo");
        stage.setScene(scene);
        stage.show();
    }
}
