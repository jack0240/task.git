/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jack1996.task;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * 启动类
 *
 * @author Jack魏
 * @since 2023-02-07
 */
public class TaskApplication extends Application {
    public static final int SCENE_WIDTH = 880;
    public static final int SCENE_HEIGHT = 660;

    /**
     * 页面启动
     *
     * @param stage 舞台
     * @throws IOException 异常
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/index-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), SCENE_WIDTH, SCENE_HEIGHT);
        stage.setTitle("我的任务");
        stage.setScene(scene);
        // 关闭窗口后程序退出
        stage.setOnCloseRequest(windowEvent -> System.exit(0));
        // 设置图标
        stage.getIcons().add(new Image(this.getClass().getResourceAsStream("/image/task.png")));
        // 显示窗口
        stage.show();
    }

    /**
     * 主方法入口
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        launch();
    }
}