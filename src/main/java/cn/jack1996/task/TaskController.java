/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jack1996.task;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * 控制类
 *
 * @author Jack魏
 * @since 2023-02-07
 */
public class TaskController {
    @FXML
    private Text showText;

    @FXML
    private TextArea taskList;

    /**
     * 开始按钮监听事件
     */
    @FXML
    protected void onStartButtonClick() {
        String textAreaValue = taskList.getText();
        if (textAreaValue == null || textAreaValue.isBlank()) {
            showText.setStyle("-fx-text-fill: red;");
            showText.setText("请填写任务列表");
        }
        final String[] split = textAreaValue.split("\n");

        List<String> startList = new ArrayList<>();
        List<String> endList = new ArrayList<>();
        List<String> nameList = new ArrayList<>();

        for (int i=0; i<split.length; i++) {
            try {
                final String sp = split[i];
                final String[] spName = sp.split("=");
                nameList.add(spName[1]);
                final String[] spTime = spName[0].split("~");
                startList.add(spTime[0]);
                endList.add(spTime[1]);
            } catch (Exception exception) {
                exception.printStackTrace();
                showText.setText("第" + (i + 1) + "行，格式有问题，请修改！");
            }
        }

        for (int i = 0; i < split.length; i++) {
            String[] startSplit = startList.get(i).split(":");
            String[] endSplit = endList.get(i).split(":");

            // 必须要开个线程跑
            new Thread(() -> {
                int start = Integer.parseInt(startSplit[0]) * 60 * 60 + Integer.parseInt(startSplit[1]) * 60;
                int end = Integer.parseInt(endSplit[0]) * 60 * 60 + Integer.parseInt(endSplit[1]) * 60;
                while (start < end) {
                    String text = String.valueOf(end - start);
                    System.out.println(text);
                    showText.setText(text);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    start++;
                }
            }).start();
        }
    }
}